//
//  MovieSearch.m
//  What2Watch
//
//  Created by Jerry on 4/23/14.
//  Copyright (c) 2014 Howard Communications, Inc. All rights reserved.
//
//

#import "AFNetworking/AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "MovieSearch.h"
#import "MovieInfo.h"

static NSOperationQueue *queue = nil;

@interface MovieSearch ()
@property (nonatomic, readwrite, strong) NSMutableArray *searchResults;
@end

@implementation MovieSearch

+ (void)initialize
{
    if (self == [MovieSearch class]) {
        queue = [[NSOperationQueue alloc] init];
    }
}

- (void)dealloc
{
   // NSLog(@"dealloc %@", self);
}

- (void)performSearchForText:(NSString *)text director:(BOOL)director completion:(SearchBlock)block
{
    if ([text length] > 0) {
        [queue cancelAllOperations];
        
        self.isLoading = YES;
        self.searchResults = [NSMutableArray arrayWithCapacity:100];
        
        NSURL *url = [self urlWithSearchText:text director:director];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        operation.responseSerializer = [AFJSONResponseSerializer serializer];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [self parseDictionary:responseObject];
            [self.searchResults sortUsingSelector:@selector(compareName:)];
            
            self.isLoading = NO;
            block(YES);
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            if (!operation.isCancelled) {
                self.isLoading = NO;
                block(NO);
            }
        }];
        
        [queue addOperation:operation];
    }
}

- (NSURL *)urlWithSearchText:(NSString *)searchText director:(BOOL)director
{
    
    NSString *escapedSearchText = [searchText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *urlString;
    
    if (director){
        urlString = [NSString stringWithFormat:@"http://itunes.apple.com/search?term=%@&limit=100&media=movie&attribute=directorTerm", escapedSearchText];
        
    } else {
        urlString = [NSString stringWithFormat:@"http://itunes.apple.com/search?term=%@&limit=100&media=movie", escapedSearchText];
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    
 //   NSLog(@"URL: %@", url);
    return url;
}

- (void)parseDictionary:(NSDictionary *)dictionary
{
    NSArray *array = dictionary[@"results"];
    if (array == nil) {
        NSLog(@"Expected 'results' array");
        return;
    }
    
    for (NSDictionary *resultDict in array) {
        
        MovieInfo *searchResult;
        
        NSString *wrapperType = resultDict[@"wrapperType"];
       // NSString *kind = resultDict[@"kind"];
		
        if ([wrapperType isEqualToString:@"track"]) {
            searchResult = [self parseMovie:resultDict];
             }
        
        if (searchResult != nil) {
            [self.searchResults addObject:searchResult];
        }
    }
}


- (MovieInfo *)parseMovie:(NSDictionary *)dictionary
{
    MovieInfo *searchResult = [[MovieInfo alloc] init];
    searchResult.name = dictionary[@"trackName"];
    searchResult.director = dictionary[@"artistName"];
    searchResult.longDescription = dictionary[@"longDescription"];
    searchResult.rating = dictionary[@"contentAdvisoryRating"];
    searchResult.artworkURL60 = dictionary[@"artworkUrl60"];
    searchResult.artworkURL100 = dictionary[@"artworkUrl100"];
    searchResult.storeURL = dictionary[@"trackViewUrl"];
    searchResult.previewURL = dictionary[@"previewUrl"];
    searchResult.releaseDate = dictionary[@"releaseDate"];
    searchResult.price = dictionary[@"trackPrice"];
    searchResult.currency = dictionary[@"currency"];
    searchResult.genre = dictionary[@"primaryGenreName"];
    return searchResult;
}


@end

