//
//  WWViewController.m
//  What2Watch
//
//  Created by Jerry on 4/23/14.
//  Copyright (c) 2014 Howard Communications, Inc. All rights reserved.
//

#import "WWViewController.h"
#import "WWMovieListController.h"
#import "MovieInfo.h"

@interface WWViewController ()

@end

@implementation WWViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.directorSearch = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)searchButtonClicked:(id)sender {
    [sender resignFirstResponder];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"MovieList"])
    {
        UINavigationController *navigationController = segue.destinationViewController;
        
        WWMovieListController *movieListController =
        (WWMovieListController *)navigationController.topViewController;
        
        movieListController.searchTerm = self.searchText.text;
        movieListController.directorSearch = self.directorSearch;
        
    }
}


- (IBAction)backgroundTap:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)searchType:(UISegmentedControl *)sender {
 //   NSLog(@"segment changed: %d", sender.selectedSegmentIndex);
    if (sender.selectedSegmentIndex == 1){
        self.directorSearch = YES;
    } else {
        self.directorSearch = NO;
    }
        
}


@end
