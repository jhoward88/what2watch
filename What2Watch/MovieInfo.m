//
//  MovieInfo.m
//  What2Watch
//
//  Created by Jerry on 4/23/14.
//  Copyright (c) 2014 Howard Communications, Inc. All rights reserved.
//

#import "MovieInfo.h"

@implementation MovieInfo

- (NSComparisonResult)compareName:(MovieInfo *)other
{
    return [self.name localizedStandardCompare:other.name];
}

@end
