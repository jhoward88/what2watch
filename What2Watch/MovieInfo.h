//
//  MovieInfo.h
//  What2Watch
//
//  Created by Jerry on 4/23/14.
//  Copyright (c) 2014 Howard Communications, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MovieInfo : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *director;
@property (nonatomic, copy) NSString *rating;
@property (nonatomic, copy) NSString *releaseDate;
@property (nonatomic, copy) NSString *artworkURL60;
@property (nonatomic, copy) UIImageView *artView;
@property (nonatomic, copy) NSString *artworkURL100;
@property (nonatomic, copy) NSString *longDescription;
@property (nonatomic, copy) NSString *storeURL;
@property (nonatomic, copy) NSString *previewURL;
@property (nonatomic, copy) NSString *currency;
@property (nonatomic, copy) NSDecimalNumber *price;
@property (nonatomic, copy) NSString *genre;

- (NSComparisonResult)compareName:(MovieInfo *)other;

@end
