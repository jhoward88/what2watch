//
//  WWViewController.h
//  What2Watch
//
//  Created by Jerry on 4/23/14.
//  Copyright (c) 2014 Howard Communications, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WWViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *searchText;
@property (nonatomic) BOOL directorSearch;

- (IBAction)searchButtonClicked:(id)sender;

- (IBAction)backgroundTap:(id)sender;
- (IBAction)searchType:(UISegmentedControl *)sender;

@end
