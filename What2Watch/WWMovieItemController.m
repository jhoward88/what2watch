//
//  WWMovieItemController.m
//  What2Watch
//
//  Created by Jerry on 4/23/14.
//  Copyright (c) 2014 Howard Communications, Inc. All rights reserved.
//

#import "UIImageView+AFNetworking.h"
#import "WWMovieItemController.h"

@interface WWMovieItemController ()

@end

@implementation WWMovieItemController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.movieInfo != nil){
        
        self.movieTitle.text = self.movieInfo.name;
        self.movieDirector.text = self.movieInfo.director;
        self.movieDescription.text = self.movieInfo.longDescription;
        self.movieRating.text = self.movieInfo.rating;

        NSString *year =  [self.movieInfo.releaseDate substringWithRange: NSMakeRange (0, 4)];
        NSString *month = [self.movieInfo.releaseDate substringWithRange: NSMakeRange (5, 2)];
        NSString *day = [self.movieInfo.releaseDate substringWithRange: NSMakeRange (8, 2)];
        NSLog (@"date = %@/%@/%@", month,day,year);

        self.movieReleaseDate.text = [NSString stringWithFormat:@"%@/%@/%@", month,day,year];
        
        [self.movieArtwork setImageWithURL:[NSURL URLWithString:self.movieInfo.artworkURL100] placeholderImage:[UIImage imageNamed:@"Placeholder"]];
        
        if( self.movieInfo.price == 0) {
            self.moviePriceCurrency.text = @"Free";
        } else {
            self.moviePriceCurrency.text = [NSString stringWithFormat:@"%@ %@",self.movieInfo.price, self.movieInfo.currency];
        }
        
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)moviePreview:(id)sender {
    
    NSString *previewURL;
    
    previewURL = self.movieInfo.previewURL;
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:previewURL ]];
}

- (IBAction)movieItunes:(id)sender {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.movieInfo.storeURL]];
    
}

- (IBAction)doneWithDetail:(id)sender {
    
   [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
}
@end
