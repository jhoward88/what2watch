//
//  WWMovieListController.h
//  What2Watch
//
//  Created by Jerry on 4/23/14.
//  Copyright (c) 2014 Howard Communications, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WWMovieListController : UITableViewController

@property NSString *searchTerm;
@property BOOL directorSearch;

- (IBAction)doneWithList:(id)sender;


@end
