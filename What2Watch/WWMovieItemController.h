//
//  WWMovieItemController.h
//  What2Watch
//
//  Created by Jerry on 4/23/14.
//  Copyright (c) 2014 Howard Communications, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MovieInfo.h"

@interface WWMovieItemController : UIViewController

@property MovieInfo *movieInfo;
@property (weak, nonatomic) IBOutlet UIImageView *movieArtwork;

@property (weak, nonatomic) IBOutlet UILabel *movieTitle;
@property (weak, nonatomic) IBOutlet UILabel *movieDirector;

@property (weak, nonatomic) IBOutlet UITextView *movieDescription;
@property (weak, nonatomic) IBOutlet UILabel *movieRating;
@property (weak, nonatomic) IBOutlet UILabel *movieReleaseDate;
@property (weak, nonatomic) IBOutlet UILabel *moviePriceCurrency;


- (IBAction)moviePreview:(id)sender;

- (IBAction)movieItunes:(id)sender;

- (IBAction)doneWithDetail:(id)sender;


@end
