//
//  WWMovieListController.m
//  What2Watch
//
//  Created by Jerry on 4/23/14.
//  Copyright (c) 2014 Howard Communications, Inc. All rights reserved.
//

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "WWMovieListController.h"
#import "WWMovieItemController.h"
#import "MovieInfo.h"
#import "MovieSearch.h"

@interface WWMovieListController ()

@end

@implementation WWMovieListController
{
    MovieSearch *movieSearch;
    MovieInfo *movieDetail;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    movieSearch = [[MovieSearch alloc] init];
    
    [movieSearch performSearchForText:self.searchTerm
                         director: self.directorSearch
                       completion:^(BOOL success) {
                           if (!success) {
                               [self showNetworkError];
                           }
                           
                           [self.tableView reloadData];
                        
                       }];
    
    [self.tableView reloadData];

}


- (void)showNetworkError
{
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"Network problem", @"Error alert: title")
                              message:NSLocalizedString(@"There was an error reading from the iTunes Store. Please try again.", @"Error alert: message")
                              delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"OK", @"Error alert: cancel button")
                              otherButtonTitles:nil];
    
    [alertView show];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (movieSearch == nil) {
        return 0;  // Not searched yet
    } else if (movieSearch.isLoading) {
        return 1;  // Loading...
    } else if ([movieSearch.searchResults count] == 0) {
        return 1;  // Nothing Found
    } else {
        return [movieSearch.searchResults count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell;
 
    if (movieSearch.isLoading) {
        cell = [tableView dequeueReusableCellWithIdentifier: @"LoadingCell" forIndexPath:indexPath];
        cell.textLabel.text = @"Loading...";
        UIActivityIndicatorView *spinner = (UIActivityIndicatorView *)[cell viewWithTag:100];
        [spinner startAnimating];
        
        
    } else if ([movieSearch.searchResults count] == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier: @"MovieItemCell" forIndexPath:indexPath];
        cell.textLabel.text = @"(No Results Found)";
        
    } else {
        //cell = [tableView dequeueReusableCellWithIdentifier: @"MovieItemCell" forIndexPath:indexPath];
        //Doing it this way to keep images from being wrongly re-used/overlaid.
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MovieItemCell" ];
        cell.userInteractionEnabled = YES;
        
        MovieInfo *searchResult = movieSearch.searchResults[indexPath.row];
        cell.textLabel.text = searchResult.name;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"Rating: %@",searchResult.rating];
        
        UIImageView *imgView=[[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 48, 48)];
        imgView.image = [UIImage imageNamed:@"Placeholder"];

        imgView.backgroundColor=[UIColor clearColor];
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        imgView.tag = indexPath.row;
        [imgView.layer setCornerRadius:8.0f];
        [imgView.layer setMasksToBounds:YES];
        
        [imgView setImageWithURL:[NSURL URLWithString:searchResult.artworkURL60] placeholderImage:[UIImage imageNamed:@"Placeholder"]];
        
        [cell.contentView insertSubview:imgView atIndex:0];
        [cell setIndentationWidth:50];
        [cell setIndentationLevel:1];
        
    
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   movieDetail = movieSearch.searchResults[indexPath.row];
  [self performSegueWithIdentifier:@"MovieItem" sender:self];
    
    
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (movieSearch.isLoading || [movieSearch.searchResults count] == 0) {
        return nil;
    } else {
        return indexPath;
    }
}


#pragma mark - Navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"MovieItem"])
    {
        UINavigationController *navigationController = segue.destinationViewController;
        
        WWMovieItemController *movieItemController =
        ( WWMovieItemController *)navigationController.topViewController;
        
   //     NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        
        movieItemController.movieInfo = movieDetail;
        
    }
}


- (IBAction)doneWithList:(id)sender {
     [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}
@end
